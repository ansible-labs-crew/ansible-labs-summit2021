+++
title = "2021"
weight = 90
+++
# Red Hat Summit 2021

[Ansible Getting Started](./ansible-getting-started/)

[Ansible automation controller Getting Started](./ansible-controller-getting-started/)

[Ansible automation controller Advanced](./ansible-controller-advanced/)

[Ansible Collections](./ansible-collections/)

[Visual Studio Code - Introduction](./vscode-intro)
